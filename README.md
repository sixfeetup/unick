# unick

## A small utility to check files for non-UTF7 characters (i.e. ASCII)

> For when you need to have a severely reduced character set

## Project setup
Install pre-commit and add the following to your pre-commit-config.yaml

```yaml
-   repo: https://bitbucket.org/sixfeetup/unick/
    rev: v0.0.6
    hooks:
    -   id: unick

```

You can test using pre-commit run --all-files.

This will check packages listed in the files in the `requirements` folder. We may need to add support for other directories.